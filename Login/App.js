import React, {Component} from 'react';
import { StyleSheet,TextInput, TouchableOpacity, Text, View, Image } from 'react-native';

export default class App extends Component{
  constructor(props){
    super(props);
    this.state = {
      textValue: '',
      count: 0,
    };
  }

  changeTextInput = text => {
    this.setState({textValue: text});
  };

  render(){
    return(
      <View style={styles.container}>

        <Image source={require('./img/logo.png')}
        style={styles.image}
        />

        <View style={styles.text}>
          <Text>Usuario:</Text>
        </View>
        <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1, marginBottom:10, width:380,}}
        onChangeText={text => this.changeTextInput(text)}
        value={this.state.textValue}
        />

        <View style={styles.text}>
          <Text>Contraseña:</Text>
        </View>
        <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1,width:380,}}
        onChangeText={text => this.changeTextInput(text)}
        value={this.state.textValue}
        />

        <TouchableOpacity style={styles.button} onPress={this.onPress}>
          <Text> Ingresar</Text>
        </TouchableOpacity>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
    paddingHorizontal: 10,
    backgroundColor: '#FACEC0',
  },
  text: {
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },

  button:{
    top:10,
    alignItems:'center',
    backgroundColor:'#B0A171',
    padding:10,
    marginTop:20,
    width:250,
    justifyContent:'center',
  },

  image:{
    width:250,
    height:250,
    marginBottom:40
  }
});


